package org.zpf.fork;

/**
 * Created by IntelliJ IDEA.
 * User : Pengfei Zhang
 * Mail : Tubetrue01@gmail.com
 * Date : 2018/5/31
 * Time : 10:54
 */
public interface Results {
    <R> R get(Object o);
}
